#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <sdktools_functions>
#include <helpers>
#include <vector>

int  i_TargetsFound, i_Targets[MAXPLAYERS];
char s_Cmd1[32], s_Cmd2[32], s_TargetNames[MAX_TARGET_LENGTH];
bool b_GodMode[MAXPLAYERS + 1], b_ML;


public Plugin:myinfo = {
	name = "Admin Extras",
	author = "J_Tanzanite",
	description = "Some small shitty additions to help manage servers.",
	version = "1.1.0",
	url = ""
};


public OnPluginStart()
{
	LoadTranslations("common.phrases.txt"); // We need this...
	RegAdminCmd("tp", Command_Teleport, ADMFLAG_SLAY, "Teleport argument1 to argument2.");
	RegAdminCmd("godmode", Command_God, ADMFLAG_SLAY, "Makes you invulnerable.");
	RegAdminCmd("nudge", Command_Nudge, ADMFLAG_SLAY, "Slightly teleports you forward.");

	HookEvent("player_builtobject", Event_BuildingMade, EventHookMode_Post);

	for (int i = 1; i < 2048; i++)
	{
		if (IsPlayerValid(i))
			SDKHook(i, SDKHook_OnTakeDamage, OnDamage);

		if (IsEntityBuilding(i))
			SDKHook(i, SDKHook_OnTakeDamage, OnDamageBuilding);
	}
}

public OnClientPutInServer(int client)
{
	b_GodMode[client] = false;
	SDKHook(client, SDKHook_OnTakeDamage, OnDamage);
}

public Action Event_BuildingMade(Event event, const char[] name, bool dontBroadcast)
{
	int building = GetEventInt(event, "index", -1);

	if (IsEntityBuilding(building))
		SDKHook(building, SDKHook_OnTakeDamage, OnDamageBuilding);
}

public Action Command_God(int client, int argc)
{
	if (!IsPlayerValid(client))
		return Plugin_Handled;

	if (argc < 1)
	{
		i_TargetsFound = 1;
		i_Targets[0] = client;
	}
	else
	{
		GetCmdArg(1, s_Cmd1, sizeof(s_Cmd1));
		i_TargetsFound = ProcessTargetString(s_Cmd1, client, i_Targets, MAXPLAYERS, 0, s_TargetNames, sizeof(s_TargetNames), b_ML);

		if (i_TargetsFound <= 0)
		{
			return Plugin_Handled;
		}
	}

	for (int i = 0; i < i_TargetsFound; i++)
	{
		b_GodMode[i_Targets[i]] = !b_GodMode[i_Targets[i]];
		PrintCenterText(i_Targets[i], "Godmode %s!", (b_GodMode[i_Targets[i]]) ? "enabled" : "disabled");
	}

	return Plugin_Handled;
}

public Action OnDamage(int victim, int& attacker, int& inflictor, float& damage, int& damagetype) 
{
	if ((b_GodMode[victim] || b_GodMode[attacker]) && attacker != victim)
	{
		if (IsPlayerValid(attacker) && !IsFakeClient(attacker))
		{
			if (b_GodMode[attacker])
				PrintCenterText(attacker, "You can't hurt others while in godmode.");
			else
				PrintCenterText(attacker, "%N has godmode enabled.", victim);
		}

		return Plugin_Handled;
	}

	return Plugin_Continue;
}


public Action OnDamageBuilding(int victim, int& attacker, int& inflictor, float& damage, int& damagetype) 
{
	if (b_GodMode[attacker])
	{
		if (IsPlayerValid(attacker) && !IsFakeClient(attacker))
			PrintCenterText(attacker, "You can't hurt buildings while in godmode.");

		return Plugin_Handled;
	}

	return Plugin_Continue;
}

public Action Command_Nudge(int client, int argc)
{
	float ang[3], vec[3], pos[3], telep[3];

	if (!IsPlayerValid(client))
		return Plugin_Handled;

	GetClientEyeAngles(client, ang);
	GetClientAbsOrigin(client, pos)
	GetAngleVectors(ang, vec, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(vec, 10.0);
	AddVectors(vec, pos, telep);
	TeleportEntity(client, telep, NULL_VECTOR, NULL_VECTOR);

	return Plugin_Handled;
}

public Action Command_Teleport(int client, int argc)
{
	int iTpTarget;
	float pos[3], ang[3], vec[3], telep[3];

	if ((!IsPlayerValid(client)) || !IsPlayerAlive(client))
		return Plugin_Handled;

	if (argc < 1) // Ignore if we don't have any targets
	{
		PrintCenterText(client, "Missing target.");
		return Plugin_Handled;
	}

	if (argc == 1)
	{
		i_TargetsFound = 1;
		i_Targets[0] = client;

		GetCmdArg(1, s_Cmd1, sizeof(s_Cmd1));
		iTpTarget = FindTarget(client, s_Cmd1, false, false);
	}
	else
	{
		GetCmdArg(1, s_Cmd1, sizeof(s_Cmd1));
		GetCmdArg(2, s_Cmd2, sizeof(s_Cmd2));

		i_TargetsFound = ProcessTargetString(s_Cmd1, client, i_Targets, MAXPLAYERS, 0, s_TargetNames, sizeof(s_TargetNames), b_ML);
		iTpTarget = FindTarget(client, s_Cmd2, false, false);
	}

	if (i_TargetsFound <= 0 || iTpTarget <= 0 || !IsPlayerValid(iTpTarget))
	{
		PrintCenterText(client, "Targets not found; Either write names properly or use \"#USERID\" from the status command.");
		return Plugin_Handled;
	}

	GetClientAbsOrigin(iTpTarget, pos);

	for (int ioffset = 0; ioffset < 180; ioffset++)
	{
		float offset = ioffset * 2.0; // This is how we spin around to check for a valid spot to place this player.
		GetClientEyeAngles(iTpTarget, ang);

		ang[0] = 0.0; // Ignore pitch.
		ang[1] += offset; // Add offset, as we want to teleport in front of the player.

		GetAngleVectors(ang, vec, NULL_VECTOR, NULL_VECTOR);
		ScaleVector(vec, 70.0);
		AddVectors(vec, pos, telep);

		if (CanClientStandHere(iTpTarget, telep) && __3dpointvisible(telep, pos))
		{
			for (int i = 0; i < i_TargetsFound; i++)
			{
				if ((IsPlayerValid(i_Targets[i])) && IsPlayerAlive(i_Targets[i]) && i_Targets[i] != client)
				{
					TeleportEntity(i_Targets[i], telep, NULL_VECTOR, NULL_VECTOR);
				}
			}

			return Plugin_Handled;
		}
	}

	PrintCenterText(client, "Couldn't find an empty spot to place your target/s, so they are now inside of you :(");
	for (int i = 0; i < i_TargetsFound; i++)
	{
		if ((IsPlayerValid(i_Targets[i])) && IsPlayerAlive(i_Targets[i]) && i_Targets[i] != client)
		{
			TeleportEntity(i_Targets[i], pos, NULL_VECTOR, NULL_VECTOR);
		}
	}		

	return Plugin_Handled;
}

bool CanClientStandHere(int client, float vec[3])
{
	float mins[3], maxs[3];

	GetClientMaxs(client, maxs);
	GetClientMins(client, mins);

	TR_TraceHullFilter(vec, vec, mins, maxs, MASK_SOLID, TraceFilter);
	if (TR_GetFraction() == 1.0)
		return true;

	return false;
}

bool TraceFilter(int entity, int mask){ return false; }

bool __3dpointvisible(float pos1[3], float pos2[3])
{
	TR_TraceRayFilter(pos1, pos2, MASK_VISIBLE, RayType_EndPoint, TraceFilter);
	return (TR_GetFraction() == 1.0) ? true : false;
}

bool IsEntityBuilding(int entity)
{
	char classname[64];

	if (IsValidEntity(entity))
	{
		GetEntityClassname(entity, classname, sizeof(classname));
		if (strcmp(classname, "obj_dispenser", false) == 0
			|| strcmp(classname, "obj_sentrygun", false) == 0
			|| strcmp(classname, "obj_teleporter", false) == 0)
			return true;
	}

	return false;
}

bool IsPlayerValid(int client)
{
	return (client > 0 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client)) ? true : false;
}